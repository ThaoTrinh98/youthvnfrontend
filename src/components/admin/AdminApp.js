import React, { Component } from 'react';
import AdminHeader from './AdminHeader';
import AdminSidebar from './AdminSidebar';
import CompanyName from '../recruitment/CompanyName';
const $ = window.jQuery;
class AdminApp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: {},
      
      pending_recuitments : [],
      total_pending_recuitments : 0,
      
    }
  }
  
  render() {
    return (
      <div className="UserApp">
        <AdminHeader/>
        <CompanyName companyName={'Welcome Thao Trinh'} />
        <div className="app-body row">
          <div className="hidden-xs hidden-sm col-md-3 col-lg-3">
             <AdminSidebar />
          </div>
                    
          <div className="Userprofile col-xs-12 col-sm-12 col-md-9 col-lg-9">           
           { /*<Breadcrumbs
                wrapperElement="ol"
                wrapperClass="breadcrumb"
                itemClass="breadcrumb-item"
                separator=""
                routes={this.props.routes}
                params={this.props.params}
           />*/}
               {this.props.children}           
        </div>
        </div>
      </div>
    );
  }
}

export default AdminApp;
