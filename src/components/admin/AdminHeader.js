import React from 'react';
import { Link, browserHistory } from 'react-router';
import moment from 'moment';
const $ = window.jQuery;

class AdminHeader extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      hidden: false,
      login: true,
      controllerBarDisplay:false
    }
  }
  toggleControllerBar =()=>{
    this.setState({controllerBarDisplay:!this.state.controllerBarDisplay});
  }
  renderNavbarRight=()=>{
      return(
     <ul className="nav navbar-nav navbar-right user" style= {{marginBottom: 10}}>
        <li><a href="#"><span className="fa fa-envelope" ></span> Message</a></li>
        <li><a href="#"><span className="fa fa-bell" ></span> Notification</a></li>
        <li><a onClick={()=>{this.toggleControllerBar()}}><span className="fa fa-caret-down"></span> Admin</a></li>
        <div className={this.state.controllerBarDisplay?"controllerBar":"controllerBar hidden"}>
            <div><Link to ="/admin">Controller Board<span className="fa fa-cog" ></span></Link></div>
            <div><Link onClick={()=>{this.setState({login:false})}}>Log Out<span className="fa fa-sign-out" ></span></Link></div>
        </div>         
     </ul>

      )
  }

  renderNavbarLeft=()=>{
    
    return(
   <ul className="nav navbar-nav navbar-right user hidden-sm hidden-md hidden-lg" style= {{marginBottom: 10}}>
       <div className="sidebar" id="#mySideNav" style ={{marginTop:60}}>
        <nav className="sidebar-nav">
          <ul className="nav">
				 				<li className="inner-child">
				 						<a><i className="fa fa-briefcase"></i>Manage User</a>
				 						<ul>
				 							<li className="inner-child"><a>Organization</a></li>
                       <ul>
                        <li ><a href="admin/verify-organization" title="" className="fa fa-dot-circle-o"style={{fontSize:13, color:"#888888" , listStyleType:"cỉrcle"}}>Verify Organization</a></li>
                        <li ><a href="/admin/list-organization" title="" className="fa fa-dot-circle-o"style={{fontSize:13, color:"#888888" , listStyleType:"cỉrcle"}}>List Organization</a></li>
                       </ul>
				 							<li><a href="#" title="">Individual</a></li>
                       <ul>
                        <li ><a href="/admin/verify-user" title="" className="fa fa-dot-circle-o"style={{fontSize:13, color:"#888888" , listStyleType:"cỉrcle"}}>Verify User</a></li>
                        <li ><a href="admin/list-CV" title="" className="fa fa-dot-circle-o"style={{fontSize:13, color:"#888888" , listStyleType:"cỉrcle"}}>List User</a></li>
                       </ul>
				 						</ul>
				 					</li>
                   <li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-file-text"></i>Manage Post</a>
				 						<ul>
				 							<li><a href="admin/verify-post" title="">Verify the Post</a></li>
											 <li><a href="/admin/list-post" title="">List Post</a></li>
                       <li><a href="/admin/recruitment/create" title="">Create Post</a></li>
				 						</ul>
				 					</li>
				 					<li className="inner-child">
				 						<a href="" title=""><i className="fa fa-money"></i>Transactions</a>
				 					</li>
				 					<li className="inner-child">
				 						<a><i className="fa fa-paper-plane"></i>Statistics</a>
				 						<ul>
				 							<li><a href="/statistics-user" title="">Statistics User</a></li>
				 							<li><a href="/statistics-post" title="">Statistics Post</a></li>
				 							<li><a href="/statistics-fee" title="">Statistics Fee</a></li>
				 						</ul>
				 					</li>
				 				
                 	<li className="inner-child"><a href="admin/feedback" title=""><i className="fa fa-close"></i>Feed Back</a></li>
				 					<li className="inner-child"><a href="admin/changepassword" title="" style={{marginTop:-20}}><i className="fa fa-user"></i>Change Password</a></li>
                   <li className="inner-child"><a href="/" title=""  style={{marginTop:-20}}><i className="fa fa-unlink"></i>Logout</a></li>
          </ul>
        </nav>
      </div>      
   </ul>

    )
  
}
  render() {
     moment.locale('vi');
   
    return (
      <nav className= "navbar navbar-inverse navbar-fixed-top" style ={{backgroundColor:"#0b1b46"}} >
        <div className="navbar-header">
            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>

             <button type="button" className="navbar-toggle pull-left visible-sm visible-xs" data-toggle="collapse" data-target="#dashboard">
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <a className="navbar-brand visible-lg" href="/"><img src="/logo/youthvn.png"/></a>
           {/* <a className="fa fa-bars" style={{color:"white", fontSize:25,marginTop: 20}}></a>*/}
            
        </div>
          <div className="collapse navbar-collapse" id="myNavbar">
            {this.renderNavbarRight()}
          </div>
          <div className="collapse navbar-collapse" id="dashboard">
            {this.renderNavbarLeft()}
          </div>
      </nav>
    )
  }
}

export default AdminHeader;
