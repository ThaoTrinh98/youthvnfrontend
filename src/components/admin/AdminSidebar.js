import React from 'react';
import ava from '../../assets/img/ava_1.png';
import {Link} from 'react-router';
// const $ = window.jQuery;

class AdminSidebar extends React.Component {
  render() {

    return (
      <div className="sidebar" id="#mySideNav" style ={{marginTop:60}}>
        <nav className="sidebar-nav hidden-xs">
          <ul className="nav">
				 				<li className="inner-child">
				 						<a><i className="fa fa-briefcase"></i>Manage User</a>
				 						<ul>
				 							<li className="inner-child"><a>Organization</a></li>
                       <ul>
                        <li ><a href="/admin/verify-organization" title="" className="fa fa-dot-circle-o"style={{fontSize:13, color:"#888888" , listStyleType:"cỉrcle"}}>Verify Organization</a></li>
                        <li ><a href="/admin/list-organization" title="" className="fa fa-dot-circle-o"style={{fontSize:13, color:"#888888" , listStyleType:"cỉrcle"}}>List Organization</a></li>
                       </ul>
				 							<li><a href="#" title="">Individual</a></li>
                       <ul>
                        <li ><a href="/admin/verify-user" title="" className="fa fa-dot-circle-o"style={{fontSize:13, color:"#888888" , listStyleType:"cỉrcle"}}>Verify User</a></li>
                        <li ><a href="/admin/list-CV" title="" className="fa fa-dot-circle-o"style={{fontSize:13, color:"#888888" , listStyleType:"cỉrcle"}}>List User</a></li>
                       </ul>
				 						</ul>
				 					</li>
                   <li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-file-text"></i>Manage Post</a>
				 						<ul>
				 							<li><a href="/admin/verify-post" title="">Verify the Post</a></li>
											 <li><a href="/admin/list-post" title="">List Post</a></li>
                       <li><a href="/admin/recruitment/create" title="">Create Post</a></li>
				 						</ul>
				 					</li>
				 					<li className="inner-child">
				 						<a href="" title=""><i className="fa fa-money"></i>Transactions</a>
				 					</li>
				 					<li className="inner-child">
				 						<a><i className="fa fa-paper-plane"></i>Statistics</a>
				 						<ul>
				 							<li><a href="/statistics-user" title="">Statistics User</a></li>
				 							<li><a href="/statistics-post" title="">Statistics Post</a></li>
				 							<li><a href="/statistics-fee" title="">Statistics Fee</a></li>
				 						</ul>
				 					</li>
				 				
                 	<li className="inner-child"><a href="/admin/feedback" title=""><i className="fa fa-close"></i>Feed Back</a></li>
				 					<li className="inner-child"><a href="/admin/changepassword" title="" style={{marginTop:-20}}><i className="fa fa-user"></i>Change Password</a></li>
                   <li className="inner-child"><a href="/" title=""  style={{marginTop:-20}}><i className="fa fa-unlink"></i>Logout</a></li>
          </ul>
        </nav>
      </div>
    )
  }
}

export default AdminSidebar;
