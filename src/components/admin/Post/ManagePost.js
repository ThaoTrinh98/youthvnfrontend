import React from 'react';
import $ from 'jquery';

export default class ManagePost extends React.Component{
      render(){
            return(
              <div className="block no-padding">
			          <div className="container">
				          <div className="row no-gape">
                  <div className="col-lg-9 column">
                    <div className="padding-left">
                      <div className="manage-jobs-sec">
                        <h3>Manage Jobs</h3>
                          <div className="extra-job-info">
                            <span><i className="fa fa-clock-o"></i><strong>9</strong> Job Posted</span>
                              <span><i className="fa fa-file-text"></i><strong>20</strong>
                              Application</span>
                                  <span><i className="fa fa-users"></i><strong>18</strong> Active Jobs</span>
                          </div>
                            <table>
                              <thead>
                                <tr>
                                      <td className="col-lg-4 colum" style={{paddingLeft:0}}>Title</td>
                                      <td className="col-lg-2 colum" style={{paddingLeft:0}}>Applications</td>
                                      <td className="col-lg-3 colum" style={{paddingLeft:0}}>Created & Expired</td>
                                      <td className="col-lg-1 colum" style={{paddingLeft:0}}>Status</td>
                                      <td className="col-lg-2 colum" style={{paddingLeft:55}}>Action</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>
                                    <div className="table-list-title">
                                      <a href="/admin/verify-post/recruitment" title="">Web Designer / Developer</a>
                                      <span><i className="fa fa-map-marker"></i>Sacramento, California</span>
                                    </div>
                                  </td>
                                  <td>
                                    <span className="applied-field">3+ Applied</span>
                                      </td>
                                      <td>
                                        <span>October 27, 2017</span><br />
                                        <span>April 25, 2011</span>
                                      </td>
                                      <td>
                                       <span className="status active">Active</span>
                                      </td>
                                      <td>
                                      <ul className="action_job">
                                        <li><span>View Job</span><a href="#" title=""><i className="fa fa-eye"></i></a></li>
                                        <li><span>Edit</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                        <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                      </ul>
                                      </td>
                                </tr>
                                  <tr>
                                    <td>
                                      <div className="table-list-title">
                                        <a href="/admin/verify-post/recruitment" title="">Web Designer / Developer</a>
                                        <span><i className="fa fa-map-marker"></i>Sacramento, California</span>
                                      </div>
                                    </td>
                                    <td>
                                       <span className="applied-field">3+ Applied</span>
                                    </td>
                                    <td>
                                      <span>October 27, 2017</span><br />
                                      <span>April 25, 2011</span>
                                    </td>
                                    <td>
                                          <span className="status active">Active</span>
                                    </td>
                                    <td>
                                      <ul className="action_job">
                                        <li><span>View Job</span><a href="#" title=""><i className="fa fa-eye"></i></a></li>
                                        <li><span>Edit</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                        <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                      </ul>
                                    </td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="table-list-title">
                                    <a href="/admin/verify-post/recruitment" title="">Web Designer / Developer</a>
                                    <span><i className="fa fa-map-marker"></i>Sacramento, California</span>
                                  </div>
                                </td>
                                <td>
                                  <span className="applied-field">3+ Applied</span>
                                </td>
                                <td>
                                   <span>October 27, 2017</span><br />
                                   <span>April 25, 2011</span>
                                </td>
                                <td>
                                  <span className="status">Inactive</span>
                                </td>
                                <td>
                                  <ul className="action_job">
                                    <li><span>View Job</span><a href="#" title=""><i className="fa fa-eye"></i></a></li>
                                    <li><span>Edit</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                    <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                  </ul>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="table-list-title">
                                    <a href="/admin/verify-post/recruitment" title="">Web Designer / Developer</a>
                                    <span><i className="fa fa-map-marker"></i>Sacramento, California</span>
                                  </div>
                                </td>
                                <td>
                                   <span className="applied-field">3+ Applied</span>
                                </td>
                                <td>
                                  <span>October 27, 2017</span><br />
                                  <span>April 25, 2011</span>
                                </td>
                                <td>
                                  <span className="status active">Active</span>
                                </td>
                                <td>
                                  <ul className="action_job">
                                    <li><span>View Job</span><a href="#" title=""><i className="fa fa-eye"></i></a></li>
                                    <li><span>Edit</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                    <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                      </ul>
                                    </td>
                                  </tr>
                                  </tbody>
                                    </table>
                              </div>
                        </div>
                  </div>
                  </div>
                  </div>
                  </div>
                 
            )
      }
}