import React from 'react';
import Record from '../../publicCV/PublicCard/PublicRecord'
import JobOverview from './../../publicCV/jobOverview';

export default class Information extends React.Component {
  render() { return (
    <div className="cand-details-sec">
      <div className="row">
        <div className="col-lg-12 column">
          <Record/>
          
        </div>
        
        
      </div>
      <div className="job-overview" style={{width:'50%'}}>
            <JobOverview/>
            
          </div>
    </div>
    
  );
  }
}