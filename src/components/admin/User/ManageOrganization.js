import React from 'react';
import $ from 'jquery';

export default class ManageOrganization extends React.Component{
      render(){
            return(
              <div className="block no-padding">
			          <div className="container">
				          <div className="row no-gape">
                  <div className="col-lg-9 column">
                    <div className="padding-left">
                      <div className="manage-jobs-sec">
                        <h3>Manage Organization</h3>
                          <div className="extra-job-info " style={{width:'300px'}}>
                            <span style={{width:'100%'}}><i className="fa fa-clock-o"></i><strong>100</strong> New organization</span>
                          </div>
                            <table>
                              <thead>
                                <tr>
                                      <td className="col-lg-4 colum" style={{paddingLeft:0}}>Name</td>
                                      <td className="col-lg-3 colum" style={{paddingLeft:0}}>Address</td>
                                      <td className="col-lg-3 colum" style={{paddingLeft:0}}>Created Date</td>
                                      
                                      <td className="col-lg-2 colum" style={{paddingLeft:55}}>Action</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>
                                    <div className="table-list-title">
                                      <a href="/admin/verify-organization/info" title="">KMSA</a><br/>
                                      
                                    </div>
                                  </td>
                                  <td>
                                    <span className="applied-field">District 1 , Ho Chi Minh</span>
                                      </td>
                                      <td>
                                        <span>October 27, 2017</span>
                                      </td>
                                      
                                      <td>
                                      <ul className="action_job">
                                      
                                        <li><span>Accept</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                        <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                      </ul>
                                      </td>
                                </tr>
                                  <tr>
                                    <td>
                                      <div className="table-list-title">
                                        <a href="/admin/verify-organization/info" title="">IBL</a><br/> 
                                      </div>
                                    </td>
                                    <td>
                                       <span className="applied-field">District 2, Ho Chi Minh</span>
                                    </td>
                                    <td>
                                      <span>October 27, 2017</span>
                                    </td>
                                   
                                    <td>
                                      <ul className="action_job">
                                      
                                        <li><span>Accept</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                        <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                      </ul>
                                    </td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="table-list-title">
                                    <a href="/admin/verify-organization/info" title="">FPT</a><br/>
                                    
                                  </div>
                                </td>
                                <td>
                                  <span className="applied-field">District 3, Ho Chi Minh</span>
                                </td>
                                <td>
                                   <span>October 27, 2017</span>
                                </td>
                                
                                <td>
                                  <ul className="action_job">
                                  
                                    <li><span>Accept</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                    <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                  </ul>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="table-list-title">
                                    <a href="/admin/verify-organization/info" title="">TMA</a><br/>
                                   
                                  </div>
                                </td>
                                <td>
                                   <span className="applied-field">District 4, Ho Chi Minh</span>
                                </td>
                                <td>
                                  <span>October 27, 2017</span>
                                </td>
                               
                                <td>
                                  <ul className="action_job">
                                  
                                    <li><span>Accept</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                    <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                      </ul>
                                    </td>
                                  </tr>
                                  </tbody>
                                    </table>
                              </div>
                        </div>
                  </div>
                  </div>
                  </div>
                  </div>
                 
            )
      }
}