import React from 'react';
import $ from 'jquery';

export default class ManageUser extends React.Component{
      render(){
            return(
              <div className="block no-padding">
			          <div className="container">
				          <div className="row no-gape">
                  <div className="col-lg-9 column">
                    <div className="padding-left">
                      <div className="manage-jobs-sec">
                        <h3>Manage User</h3>
                          <div className="extra-job-info " style={{width:'300px'}}>
                            <span style={{width:'100%'}}><i className="fa fa-clock-o"></i><strong>100</strong> New User</span>
                          </div>
                            <table>
                              <thead>
                                <tr>
                                      <td className="col-lg-4 colum" style={{paddingLeft:0}}>Name</td>
                                      <td className="col-lg-3 colum" style={{paddingLeft:0}}>University</td>
                                      <td className="col-lg-3 colum" style={{paddingLeft:0}}>Created Date</td>
                                      
                                      <td className="col-lg-2 colum" style={{paddingLeft:55}}>Action</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>
                                    <div className="table-list-title">
                                      <a href="/admin/verify-user/CV-user" title="">Nguyen Van A</a><br/>
                                      <span><i className="fa fa-map-marker"></i>District 1, Ho Chi Minh</span>
                                    </div>
                                  </td>
                                  <td>
                                    <span className="applied-field">Bach Khoa university</span>
                                      </td>
                                      <td>
                                        <span>October 27, 2017</span>
                                      </td>
                                      
                                      <td>
                                      <ul className="action_job">
                                      
                                        <li><span>Accept</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                        <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                      </ul>
                                      </td>
                                </tr>
                                  <tr>
                                    <td>
                                      <div className="table-list-title">
                                        <a href="/admin/verify-user/CV-user" title="">Nguyen Van A</a><br/> <span><i className="fa fa-map-marker"></i>District 1, Ho Chi Minh</span>
                                      </div>
                                    </td>
                                    <td>
                                       <span className="applied-field">Bach Khoa university</span>
                                    </td>
                                    <td>
                                      <span>October 27, 2017</span>
                                    </td>
                                   
                                    <td>
                                      <ul className="action_job">
                                      
                                        <li><span>Accept</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                        <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                      </ul>
                                    </td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="table-list-title">
                                    <a href="/admin/verify-user/CV-user" title="">Nguyen Van A</a><br/>
                                    <span><i className="fa fa-map-marker"></i>District 1, Ho Chi Minh</span>
                                  </div>
                                </td>
                                <td>
                                  <span className="applied-field">Bach Khoa university</span>
                                </td>
                                <td>
                                   <span>October 27, 2017</span>
                                </td>
                                
                                <td>
                                  <ul className="action_job">
                                  
                                    <li><span>Accept</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                    <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                  </ul>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="table-list-title">
                                    <a href="/admin/verify-user/CV-user" title="">Nguyen Van A</a><br/>
                                    <span><i className="fa fa-map-marker"></i>District 1, Ho Chi Minh</span>
                                  </div>
                                </td>
                                <td>
                                   <span className="applied-field">Bach Khoa university</span>
                                </td>
                                <td>
                                  <span>October 27, 2017</span>
                                </td>
                               
                                <td>
                                  <ul className="action_job">
                                  
                                    <li><span>Accept</span><a href="#" title=""><i className="fa fa-pencil"></i></a></li>
                                    <li><span>Delete</span><a href="#" title=""><i className="fa fa-trash-o"></i></a></li>
                                      </ul>
                                    </td>
                                  </tr>
                                  </tbody>
                                    </table>
                              </div>
                        </div>
                  </div>
                  </div>
                  </div>
                  </div>
                 
            )
      }
}