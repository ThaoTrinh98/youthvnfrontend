import React from 'react';

import Select from 'react-select-plus';
import CheckBox3 from '../filter/Checkboxes.js';
import Search3 from '../filter/SearchBar.js';
import Select3 from '../filter/SelectBar.js';
import Ranges from '../filter/Ranges.js';
import Slider from '../filter/Sliders.js';

function logChange(val) {
  console.log( val);
}
export default class CVfilter extends React.Component{
  state = {
    school: '',
    field: '',
    specialization: '',
    province: '',
  }


 
  
  handleChangeSchool = (school, key) => {
    this.setState({[key]: school });
   
    console.log(`Selected: ${school.label}`);
  }
 
  handleChangeField = (field, key) => {
    this.setState({[key]: field });
   
    console.log(`Selected: ${field.label}`);
  }

  handleChangeSpecialization = (specialization, key) => {
    this.setState({[key]: specialization });
   
    console.log(`Selected: ${specialization.label}`);
  }

  handleChangeProvince = (province, key) => {
    this.setState({[key]: province });
   
    console.log(`Selected: ${province.label}`);
  }

  
  render(){
    const { school, field, specialization, province } = this.state;
    const { selectedOption } = this.state;
  	const value1 = school && school.value;
    const value2 = field && field.value;
    const value3 = specialization && specialization.value;
    const value4 = province && province.value;
    return(
          <div className="filter">
            <div >
              <Search3
                placeholder={"Search Keywords"}
                icon={"fa fa-search"}
              />
            </div>
             
            <div style={{marginTop:50, marginBottom:-200}}>
              <CheckBox3
                valueArr={['Male','Female']}
                groupName={'Gender'}
                name={'Gender'}
                multi={false}
                
              />
            </div>

            <CheckBox3
                valueArr={['University','College','Intermediate']}
                groupName={'Degree'}
                name={'Degree'}
                multi={true}/>
            
            <Select3 
              name="Select school"
              groupName="Select school"
              value={value1}
              onChange={(school) => this.handleChangeSchool(school, 'school')}
              options={[
                { value: 'Bach Khoa University', label: 'Bach Khoa University' },
                { value: 'University Infomation Technology', label: 'University Infomation Technology' },
                { value: 'University Science and Technology', label: 'University Science and Technology' },
              ]}
              multi={true}
      />

            <div style={{marginTop:'5%'}}>
      <Select3 
              name="Select field"
              groupName="Select field"
              value={value2}
              onChange={(field) => this.handleChangeField(field, 'field')}
              options={[
                { value: 'Marketing', label: 'Marketing' },
                { value: 'IT', label: 'IT' },
                { value: 'design', label: 'design' },
              ]}
              multi={true}
      />
</div>
      <Select3 
              name="Select specialization"
              groupName="Select specialization"
              value={value3}
              onChange={(specialization) => this.handleChangeSpecialization(specialization, 'specialization')}
              options={[
                { value: 'Computer Science', label: 'Computer Science' },
                { value: 'Information Technology', label: 'Information Technology' },
                
              ]}
              multi={true}
      />

      <Select3 
              name="Select province"
              groupName="Select province"
              value={value4}
              onChange={(province) => this.handleChangeProvince(province, 'province')}
              options={[
                { value: 'Ho Chi Minh', label: 'Ho Chi Minh' },
                { value: 'Phu Yen', label: 'Phu Yen' },
                
              ]}
              multi={true}
      />
            
           
           <div>
            <Ranges
            name={"Age"}
            defaultValue={[0,60]}
            min={0}
            max={60}
            allowCross={false}
            step={1}
           />
           </div>
           <hr/>
            <Slider
            name={"Experience"}
            defaultValue={0}
            min={0}
            max={10}
            step={1}
            />
         </div>
         
         
          
          

        
   
    );    
  }
}