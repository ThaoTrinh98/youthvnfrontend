import React from 'react';
import Introduce from './introduce.js';
import Portfolio from './portfolio.js';
import FollowTime from './followTime.js'
import FollowItem from './followItem.js'
import FollowRange from './followRange.js'

export default class CVRecord extends React.Component{
      render(){
            return(
      <div className="cand-details" id="about">
            
          <h2>Candidates About</h2>
           
            <Introduce
              name="Introduction"
              groupName="Introduce"
            />

            <FollowTime
              name="Experience"
              groupName="Experience"
              id="experience"
              valueArr={[
                {
                  year:"12/12/2016 - 12/12-2027",
                  name:"Web designer",
                  description:"React & Nodejs",
                  
                },
        
                {
                  year:"26/12/2027 - 13/9/2030",
                  name:"Unior",
                  description:"Blockchain",
                 
                }
              ]}
            />

           <FollowTime
              name="Project"
              groupName="Project"
              id="project"
              valueArr={[
                {
                  year:"2000 - 2001",
                  name:"YouthVN",
                  description:"React & Nodejs",
                  
                },
        
                {
                  year:"2001 - 2002",
                  name:"Fintech",
                  description:"Blockchain",
                 
                }
              ]}
            />

           <FollowRange
              name="Skills"
              groupName="Skills"
              id="skills"
              valueArr={[
                {
                 
                  name:"React",
                  level:"80%",

                  
                },
        
                {
                  name:"Nodejs",
                  level:"70%"
                 

                 
                }
              ]}
            />

              <FollowItem
              name="Language"
              groupName="Language"
              id="language"
              valueArr={[
                {
                 
                  name:"English",
                  level:"Level: Toeic 700",
                  icon:"fa fa-language",

                  
                },
        
                {
                  name:"Japanese",
                  level:"Level: N2",
                  icon:"fa fa-language",

                 
                }
              ]}
            />

             <FollowItem
              name="Education"
              groupName="Education"
              id="education"
              valueArr={[
                {
                 
                  name:"University",
                  year:"12/2020",
                  place:"Bach Khoa University",
                  level:"Computer Science",
                  icon:"fa fa-graduation-cap",

                  
                },
        
                {
                  name:"High School",
                  year:"12/2016",
                  place:"Le Thanh Phuong",
                  level:"12/12",
                  icon:"fa fa-graduation-cap",
                 
                }
              ]}
            />

              <FollowItem
              name="Degree"
              groupName="Degree"
              id="degree"
              valueArr={[
                {
                 
                  name:"Engineer Dgree",
                  place:"Bach Khoa University",
                  level:"Good",
                  icon:"fa fa-star",

                  
                },
        
                {
                  name:"Master Degree",
                  place:"AAAAA School",
                  level:"Good",
                  icon:"fa fa-star",
                 
                }
              ]}
            />


            <FollowTime
              name="Awards"
              groupName="Awards"
              id="awards"
              valueArr={[
                {
                  year:"12/12/2016",
                  name:"First prize in web",
                  description:"React & Nodejs",
                  
                },
        
                {
                  year:"26/12/2017",
                  name:"First prize in Blockchain",
                  description:"Blockchain",
                 
                }
              ]}
            />

            
            
            <FollowItem
              name="Activities"
              groupName="Activities"
              id="activities"
              valueArr={[
                {
                 
                  name:"Volunteer",
                  year:"2016-2017",
                  icon:"fa fa-calendar",
                  place:"Malaysia",
                  description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin a ipsum tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus"
                  
                },
        
               
              ]}
            />
            

             <Portfolio
              name="Portfolio"
              groupName="Portfolio"
              id="portfolio"
              valueArr={[
                {
                 
                  link:"http://placehold.it/165x115",
                  
                },
        
                {
                 
                  link:"http://placehold.it/165x115",
                  
                },
                {
                 
                  link:"http://placehold.it/165x115",
                  
                },
                {
                 
                  link:"http://placehold.it/165x115",
                  
                }
              ]}
            />

            <FollowTime
              name="Publication"
              groupName="Publication"
              id="publication"
              valueArr={[
                {
                  year:"2000",
                  name:"Author: Mr Thanh",
                  description:"How to get job?",
                  place:"Ho Chi Minh"
                },
        
                {
                  year:"2001",
                  name:"Author: Mr Cong",
                  description:"How to be successful?",
                  place:"Ho Chi Minh"
                }
              ]}
            />

               <FollowItem
              name="Recommendator"
              groupName="Recommendator"
              id="recommendator"
              valueArr={[
                {
                 
                  name:"Mr. Been",
                  place:"Been@gmail.com",
                  description:"Relation: Old Boss",
                  position:"CEO KMS company",
                  
                  icon:"fa fa-suitcase",

                  
                },
        
                {
                  name:"Mr. Hoa",
                  place:"Hoa@gmail.com",
                  description:"Relation: Friend",
                  position:"CEO FPT company",
                  
                  icon:"fa fa-suitcase",
                 
                }
              ]}
            />
             
           
          </div>
            )
      }
}