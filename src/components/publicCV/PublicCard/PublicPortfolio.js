
import React from 'react';
import $ from 'jquery';
import { Link } from 'react-router';

class ListofItem extends React.Component {
      render() {
          return (

            <div className="mp-col">
              <div className="mportolio"><img src={this.props.link} alt="" /><a href="#" title=""><i className="fa fa-search"></i></a></div>
            </div>
          );
      }
    }




export default class FollowItem extends React.Component {

      constructor(props){
        super(props);
        this.state = {
          icondown: true,
    
         
        }
      }
    
    
      toggle_widget() {
        $(document.getElementById(this.props.groupName)).next().slideToggle();
        document.getElementById(this.props.groupName).classList.toggle('active');
        document.getElementById(this.props.groupName).classList.toggle('closed');
        this.setState({icondown:!this.state.icondown});
      }
    
      toggleIcon=()=>{
        this.setState({icondown:!this.state.icondown});
      }
    
      renderList(valueArr){
            return valueArr.map(elem=>
                <ListofItem link={elem.link}/>
          );
      }
    
      render(){
          const func = this.renderList(this.props.valueArr);
          const { create} = this.props;
        return (
    
         
          <div className="mini-portfolio" id={this.props.id}>
          <div className="row">
           
            <h2 className="sb-title open"
              id={this.props.groupName}
              onClick={() => this.toggle_widget()}
              >{this.props.name}
              <b onClick  = {()=>{this.toggleIcon()}}className={this.state.icondown?'fa fa-sort-up':'fa fa-sort-down'}>
              </b>
            </h2>
            <div className="specialism_widget">
              <div className="mp-row">
                {func}
              </div>
              </div>
            </div>
            
        </div>
             
          
           
        );
        ;
      }
    }