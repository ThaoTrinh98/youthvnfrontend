


import React from 'react';
import $ from 'jquery';
import { Link } from 'react-router';

export class ElementofItem extends React.Component {

      render() {
        const {create} = this.props;
          return (

            <div className="edu-history">
                <i className={this.props.icon}></i>
                  <div className="edu-hisinfo">
                    <div className="row">
                      
                        <span style={{color:'#8b91dd',marginLeft:"2.5%"}}>{this.props.name} - </span>
                          <span><i>{this.props.year}</i></span>
                          <span><i>{this.props.position}</i></span>
                      </div>
                        
                      

                        <i> <span>{this.props.place}</span></i>
                        <i> <span>{this.props.level}</span></i>
                        <i> <span>{this.props.description}</span></i> 
                        <i> <span>{this.props.email}</span></i>
                        <i> <span>{this.props.relation}</span></i> 
                  </div>
            </div>
          );
      }
    }


export class ElementofRange extends React.Component {
      render() {
        const { create} = this.props;
          return (

            <div className="progress-sec">
               <div className="row">
                  
                      <span style={{marginLeft:"2.5%"}}>{this.props.name} - </span>
                      <span>{this.props.level}</span>
                  </div>
                  
            <div className="progressbar"> <div className="progress " style={{width: this.props.level}}></div> </div>
    </div>
          );
      }
    }



export class ElementofTime extends React.Component {
  
      render() {
        const { create} = this.props;
          return (
            <div className="edu-history style2">
            <i></i>
            <div className="edu-hisinfo">
              <div className="row">
              
                  <span style={{color:'#8b91dd',marginLeft:"2.5%"}}>{this.props.year}</span>
                </div>
               
              <span style={{fontSize:14, color: "#888888"}}> {this.props.name}</span>
            <span style={{fontSize:14, color: "#888888"}}> {this.props.description}</span>
            <span style={{fontSize:14, color: "#888888"}}> {this.props.place}</span>
            </div>
          </div>
          );
      }
    }
    