


import React from 'react';
import $ from 'jquery';
import { Link } from 'react-router';
import {ElementofItem} from './PublicRenderItem' 

export default class FollowItem extends React.Component {

      constructor(props){
        super(props);
        this.state = {
          icondown: true,
    
         
        }
      }
    
    
      toggle_widget() {
        $(document.getElementById(this.props.groupName)).next().slideToggle();
        document.getElementById(this.props.groupName).classList.toggle('active');
        document.getElementById(this.props.groupName).classList.toggle('closed');
        this.setState({icondown:!this.state.icondown});
      }
    
      toggleIcon=()=>{
        this.setState({icondown:!this.state.icondown});
      }

      render(){
         const valueArr = this.props.valueArr;
          const { create} = this.props;
        return (
    
         
            <div className="edu-history-sec" id={this.props.id}>
            <div className="row">
              <h2 className="sb-title open"
                id={this.props.groupName}
                onClick={() => this.toggle_widget()}
                >{this.props.name}
                <b onClick  = {()=>{this.toggleIcon()}}className={this.state.icondown?'fa fa-sort-up':'fa fa-sort-down'}>
                </b>
      
                
              </h2>
      
              <div className="specialism_widget">
              

               {valueArr.map(u=>
                  <ElementofItem year={u.year}
                              name={u.name}
                              level={u.level}
                              place={u.place}
                              icon={u.icon}
                              position={u.position}
                              email={u.email}
                              relation={u.relation}
                              description={u.description}
                              create={this.props.create}
               />)}
          
               
                </div>
                
              </div>
            </div>

        );
        ;
      }
    }