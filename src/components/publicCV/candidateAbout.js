import React from 'react';
import JobOverview from './jobOverview.js';
import Contact from './contact.js';
import ListItem from './ListItem.js'
import Sliders from './sliderItem.js'
import RelatedCandidate from './relatedCandidate.js'
import Record from './PublicCard/PublicRecord'
import CV from '../userCMS/views/Account/CV/CV'
export default class Information extends React.Component {
  render() { return (
    <div className="cand-details-sec">
      <div className="row">
        <div className="col-lg-7 column col-xs-12 candidateCV">

           
           <Record/>
           
          
        </div>
        <div className="col-lg-5 column">
        <div className="col-lg-10 column">
          <div className="job-overview">
            <JobOverview/>
            <Contact/>
          </div>    
        </div>
        <div className="col-lg-12 column">
        <h2 style={{marginLeft:15}}>Related Candidates</h2>
          <RelatedCandidate/>
        </div>
        </div>
        
        
      </div>
    </div>
    
  );
  }
}