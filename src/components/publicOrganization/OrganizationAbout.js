import React, { Component } from 'react'
import { Link } from 'react-router';
import _ from 'lodash';
import request from 'superagent';
import loading from '../../assets/icon/Rolling.gif';
import OrganizationFigures from './OrganizationFigures';
import OrganizationDetail from './OrganizationDetail';
import ListRecruitment from '../recruitmentHolder/ListRecruitment';
class OrganizationAbout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      organization: {}
    }
  }
  
  render() {
   

    
    return (
      <div className='PublicOrganization'> 
        <div className="container body">
      <div className='OrganizationInfo'>
         <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <OrganizationFigures />
            <OrganizationDetail />
        </div>

        
      </div>
      </div>
      </div>
    )
  }
}

export default OrganizationAbout