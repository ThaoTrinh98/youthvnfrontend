import React from 'react';
import $ from 'jquery';
import CompanyName from'./CompanyName';
import CompanyInfoCard from './CompanyInfoCard';
import Job from './Job';
import InfoRecruitment from './InfoRecruitment';
/*import '../assets/oldcss/css/style.css';
import '../assets/oldcss/css/responsive.css';
import '../assets/oldcss/css/animate.min.css';
import '../assets/oldcss/css/bootstrap-grid.css';
import '../assets/oldcss/css/icons.css';
import '../assets/oldcss/css/colors/colors.css';*/


class Recruitment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            heart_color: '#B8B8B8',

            companyName:'Company',

            

        }
    }
  
    render() {
    const{companyName}= this.state;
        return (
            <div>
                <CompanyName companyName={this.props.companyName} companyImage={this.props.companyImage} />
                <section>
                   <InfoRecruitment/>
                </section>
            </div>
        );
    }
}
export default Recruitment;