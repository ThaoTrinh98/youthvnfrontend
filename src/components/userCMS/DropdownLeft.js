import React from 'react';
import { Link, browserHistory } from 'react-router';
import swal from 'sweetalert';
import moment from 'moment';
import { MoneyToNumber, formattingItemArr, formattingMoney } from '../../commons/share';
import { ACCEPTED_TYPES, INVITATION_TYPES } from '../../commons/constants';
const $ = window.jQuery;

class DropdownLeft extends React.Component {
 

  render() {
     moment.locale('vi');
    
    return (
       <div className="dropdown">
            <button className="dropdown-toggle" id="menu1" data-toggle="dropdown"><i className="fa fa-bars"></i>
            </button>

            <ul className="dropdown-menu" role="menu" aria-labelledby="menu1">
            <div className="sidebar" id="#mySideNav" >
        <nav className="sidebar-nav">
          <ul className="nav">
            	<li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-file-text"></i>User Profile</a>
										 <ul>
										 <li><a href="/user/account/profile" title="">My Profile</a></li>
				 							<li><a href="/user/account/cv" title="">My CV</a></li>
				 							<li><a href="/user/account/changepassword" title="">Change Password</a></li>
				 							<li><a href="/user/account/deleteaccount" title="">Remove Account</a></li>
				 						</ul>
				 					</li>
				 					<li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-briefcase"></i>Manage Organization</a>
				 						<ul>
				 							<li><a href="/user/organization/create" title="">Create Organization</a></li>
				 							<li><a href="#" title="">My Organization</a></li>
				 							
				 						</ul>
				 					</li>
				 					<li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-money"></i>Transactions</a>
				 						<ul>
				 							<li><a href="#" title="">History Transactions</a></li>
				 						</ul>
				 					</li>
				 					<li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-paper-plane"></i>Jobs Management</a>
				 						<ul>	 						
				 							<li><a href="/user/recruitment/create" title="">Post a new job</a></li>
				 							<li><a href="#" title="">Posted Jobs List</a></li>
				 							<li><a href="#" title="">Applied Jobs List</a></li>
				 						</ul>
				 					</li>
				 					<li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-user"></i>Others</a>
				 						<ul>
				 							<li><a href="#" title="">Social Network</a></li>
				 							<li><a href="#" title="">Notification</a></li>
				 						</ul>
				 					</li>		

				 					<li className="inner-child"><a href="#" title=""><i className="fa fa-unlink"></i>Logout</a></li>
          </ul>
        </nav>
      </div>
            </ul>
       </div>
    )
  }
}

export default DropdownLeft;
