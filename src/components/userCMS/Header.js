import React from 'react';
import { Link, browserHistory } from 'react-router';
import moment from 'moment';
const $ = window.jQuery;

class Header extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      hidden: false,
      login: true,
      controllerBarDisplay:false
    }
  }
  toggleControllerBar =()=>{
    this.setState({controllerBarDisplay:!this.state.controllerBarDisplay});
  }
  renderNavbarRight(){
    if (!this.state.login)
    return (
   <ul className="nav navbar-nav navbar-right">
      <button type="button" class="btn btn-default">+ Post Job</button>
      <li><a href="#" onClick ={()=>{this.props.fadeInSignUp()}}><span className="fa fa-key"></span> Sign Up</a></li>
      <li><a href="#" onClick ={()=>{this.props.fadeInSignIn()}}><span className="fa fa-sign-in"></span> Login</a></li>
   </ul>

    )
    else 
    return(
    <ul className="nav navbar-nav navbar-right user">


    <ul className="nav navbar-nav navbar-right user visible-xs visible-sm">
      <li><a href="#"><span className="fa fa-envelope" ></span> Message</a></li>
      <li><a href="#"><span className="fa fa-bell" ></span> Notification</a></li>
      <li><a onClick={()=>{this.toggleControllerBar()}}><span className="fa fa-caret-down"></span> User</a></li>
      <div className={this.state.controllerBarDisplay?"controllerBar":"controllerBar hidden"}>
      <Link style={{color:'#ee2a6b'}}><span className="fa fa-users" style={{marginLeft:10}}></span>Call Employers</Link>
          <Link style={{color:'#ee2a6b'}} to ="/user"><span className="fa fa-cog" style={{marginLeft:10}}></span>Controller Board</Link>
          <Link style={{color:'#ee2a6b'}} onClick={()=>{this.setState({login:false})}}><span className="fa fa-sign-out" style={{marginLeft:10}} ></span>Log Out</Link>
      </div>         
   </ul>
      <ul className="nav navbar-nav hidden-xs hidden-sm">

            <li className="a1">
              <Link to=""> <span><i className="fa fa-envelope"></i></span>Message</Link>
            </li>

            <li className="a1">
              <Link to=""> <span><i className="fa fa-bell"></i></span>Notification</Link>
            </li>

            <li className="a1">
              <Link to=""> <span><i className="fa fa-caret-down"></i></span>User</Link>
              <ul>
                  <li><Link >Call Employers<span className="fa fa-users" ></span></Link></li>
                  <li><Link to ="/user">Controller Board<span className="fa fa-cog"User ></span></Link></li>
                  <li><Link onClick={()=>{this.setState({login:false})}}>Log Out<span className="fa fa-sign-out" ></span></Link></li>
             </ul>
          </li>

      </ul>     
   </ul>
   
    )

 }

  renderNavbarLeft=()=>{
    return(
   <ul className="nav navbar-nav navbar-right user hidden-sm hidden-md hidden-lg" style= {{marginBottom: 10}}>
     <div className="sidebar" id="#mySideNav" style ={{marginTop:60}}>
        <nav className="sidebar-nav">
          <ul className="nav">
            	<li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-file-text"></i>User Profile</a>
										 <ul>
										 <li><a href="/user/account/profile" title="">My Profile</a></li>
				 							<li><a href="/user/account/cv" title="">My CV</a></li>
				 							<li><a href="/user/account/changepassword" title="">Change Password</a></li>
				 							<li><a href="/user/account/deleteaccount" title="">Remove Account</a></li>
				 						</ul>
				 					</li>
				 					<li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-briefcase"></i>Manage Organization</a>
				 						<ul>
				 							<li><a href="/user/organization/create" title="">Create Organization</a></li>
				 							<li><a href="#" title="">My Organization</a></li>
				 							
				 						</ul>
				 					</li>
				 					<li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-money"></i>Transactions</a>
				 						<ul>
				 							<li><a href="#" title="">History Transactions</a></li>
				 						</ul>
				 					</li>
				 					<li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-paper-plane"></i>Jobs Management</a>
				 						<ul>	 						
				 							<li><a href="/user/recruitment/create" title="">Post a new job</a></li>
				 							<li><a href="#" title="">Posted Jobs List</a></li>
				 							<li><a href="#" title="">Applied Jobs List</a></li>
				 						</ul>
				 					</li>
				 					<li className="inner-child">
				 						<a href="#" title=""><i className="fa fa-user"></i>Others</a>
				 						<ul>
				 							<li><a href="#" title="">Social Network</a></li>
				 							<li><a href="#" title="">Notification</a></li>
				 						</ul>
				 					</li>		

				 					<li className="inner-child"><a href="#" title=""><i className="fa fa-unlink"></i>Logout</a></li>
          </ul>
        </nav>
      </div>      
   </ul>

    )
}

  render() {
     moment.locale('vi');
   
    return (
      <nav className= "navbar navbar-inverse navbar-fixed-top" style ={{backgroundColor:"#0b1b46"}} >
        <div className="navbar-header">
            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>

             <button type="button" className="navbar-toggle pull-left visible-sm visible-xs" data-toggle="collapse" data-target="#dashboard">
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <a className="navbar-brand visible-lg" href="/"><img src="/logo/youthvn.png"/></a>
           
        </div>
          <div className="collapse navbar-collapse" id="myNavbar">
            {this.renderNavbarRight()}
          </div>
          <div className="collapse navbar-collapse" id="dashboard">
            {this.renderNavbarLeft()}
          </div>
      </nav>
    )
  }
}

export default Header;
