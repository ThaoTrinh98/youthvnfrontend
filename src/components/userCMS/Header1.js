import React from 'react';
import { Link, browserHistory } from 'react-router';
import swal from 'sweetalert';
import moment from 'moment';
import { MoneyToNumber, formattingItemArr, formattingMoney } from '../../commons/share';
import { ACCEPTED_TYPES, INVITATION_TYPES } from '../../commons/constants';
import DropdownLeft from './DropdownLeft'
import DropdownRight from './DropdownRight'
const $ = window.jQuery;

class Header extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      hidden: false,
      login: true,
      controllerBarDisplay:false
    }
  }
  toggleControllerBar =()=>{

    this.setState({controllerBarDisplay:!this.state.controllerBarDisplay});

  }
  setBackgroundHeader=()=>{
   
    if(window.scrollY >80 ){
      this.setState({hidden: true});
    }
      else{
        this.setState({hidden:false});
      }
  }
  componentDidMount(){
    window.addEventListener("scroll",this.setBackgroundHeader);
  }
  
   renderNavbarRight(){
      if (!this.state.login)
      return (
     <ul className="nav navbar-nav navbar-right">
        <button type="button" class="btn btn-default">+ Post Job</button>
        <li><a href="#" onClick ={()=>{this.props.fadeInSignUp()}}><span className="fa fa-key"></span> Sign Up</a></li>
        <li><a href="#" onClick ={()=>{this.props.fadeInSignIn()}}><span className="fa fa-sign-in"></span> Login</a></li>
     </ul>

      )
      else 
      return(
      <ul className="nav navbar-nav navbar-right user">


      <ul className="nav navbar-nav navbar-right user visible-xs visible-sm">
        <li><a href="#"><span className="fa fa-envelope" ></span> Message</a></li>
        <li><a href="#"><span className="fa fa-bell" ></span> Notification</a></li>
        <li><a onClick={()=>{this.toggleControllerBar()}}><span className="fa fa-caret-down"></span> User</a></li>
        <div className={this.state.controllerBarDisplay?"controllerBar":"controllerBar hidden"}>
        <Link style={{color:'#ee2a6b'}}><span className="fa fa-users" style={{marginLeft:10}}></span>Call Employers</Link>
            <Link style={{color:'#ee2a6b'}} to ="/user"><span className="fa fa-cog" style={{marginLeft:10}}></span>Controller Board</Link>
            <Link style={{color:'#ee2a6b'}} onClick={()=>{this.setState({login:false})}}><span className="fa fa-sign-out" style={{marginLeft:10}} ></span>Log Out</Link>
        </div>         
     </ul>
        <ul className="nav navbar-nav hidden-xs hidden-sm">

              <li className="a1">
                <Link to=""> <span><i className="fa fa-envelope"></i></span>Message</Link>
              </li>

              <li className="a1">
                <Link to=""> <span><i className="fa fa-bell"></i></span>Notification</Link>
              </li>

              <li className="a1">
                <Link to=""> <span><i className="fa fa-caret-down"></i></span>User</Link>
                <ul>
                    <li><Link >Call Employers<span className="fa fa-users" ></span></Link></li>
                    <li><Link to ="/user">Controller Board<span className="fa fa-cog"User ></span></Link></li>
                    <li><Link onClick={()=>{this.setState({login:false})}}>Log Out<span className="fa fa-sign-out" ></span></Link></li>
		           </ul>
            </li>

        </ul>     
     </ul>
     
      )

   }
   render() {
     moment.locale('vi');
    console.log(this.state.controllerBarDisplay);
    return (
      <nav className= {this.state.hidden?"navbar-hidden navbar-inverse navbar-fixed-top":"navbar navbar-inverse navbar-fixed-top"} >
        <div className="navbar-header">
            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target={this.state.hidden?"#myNavbar-hidden":"#myNavbar"}>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <a className="navbar-brand visible-lg" href="/"><img src="/logo/youthvn.png"/></a>
        </div>
          <div className="collapse navbar-collapse" style={{paddingBottom:10}} id={this.state.hidden?"myNavbar-hidden":"myNavbar"}>
            <ul className="nav navbar-nav visible-sm visible-md ">
            <DropdownLeft/> 
            </ul>
            <DropdownRight />      
           
            {this.renderNavbarRight()}
          </div>
      </nav>
    )
  }
}

export default Header;




