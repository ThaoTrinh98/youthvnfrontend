import React, { Component } from 'react';
import Breadcrumbs from 'react-breadcrumbs';
import {Link, browserHistory} from 'react-router';
import Header from './Header';
import Header1 from './Header1'
import Footer from '../fixedComponents/Footer';
import Sidebar from './Sidebar';
import CompanyName from '../recruitment/CompanyName';
class UserApp extends Component {
  constructor(props) {
    super(props);
   
  }
  
  render() {
    

    return (
      <div className="UserApp">
        <div className="visible-xs visible-lg">
            <Header/>
        </div>
        <div className="visible-sm visible-md">
        <Header1/>
        </div>
        <CompanyName companyName={'Welcome User'} />
        <div className="app-body row">
          <div className="hidden-xs hidden-sm hidden-md col-lg-3">
             <Sidebar />
          </div>
                    
          <div className="Userprofile col-xs-12 col-sm-12 col-md-12 col-lg-9">           
           { /*<Breadcrumbs
                wrapperElement="ol"
                wrapperClass="breadcrumb"
                itemClass="breadcrumb-item"
                separator=""
                routes={this.props.routes}
                params={this.props.params}
           />*/}
               {this.props.children}           
        </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default UserApp;
