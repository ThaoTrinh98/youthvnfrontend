import React, { Component } from 'react';
import Introduce from './introduce.js';
import Education from './education.js';
import Experience from './experience.js';
import Portfolio from './portfolio.js';
import ListSkill from './listSkill.js';
import Awards from './awards.js';
import Course from './course.js';
import Project from './project.js'
import Activities from './activities.js'
import Publication from './publication.js'
import Language from './language.js'
import Degree from './degree.js'
import Recommendator from './recommendator.js'
import CVRecord from '../../../publicCV/CVRecord.js'

class CV extends Component {
    constructor(props) {
      super(props);
     
    }
    
    render() {
      
  
      return (
        <div className="CV">
                <CVRecord/>
        </div>
      );
    }
  }
  
  export default CV;