import React, { Component } from 'react';

import Portfolio from './portfolio.js';
import ExperienceCard from '../../Card/ExperienceCard'
import EducationCard from '../../Card/EducationCard'
import SkillCard from '../../Card/SkillCard'
import PortfolioCard from '../../Card/PortfolioCard'
import ProjectCard from '../../Card/ProjectCard'
import PublicationCard from '../../Card/PublicationCard'
import DegreeCard from '../../Card/DegreeCard'
import AwardCard from '../../Card/AwardCard'
import ActivityCard from '../../Card/ActivityCard'
import RecommendatorCard from '../../Card/RecommendatorCard'
import LanguageCard from '../../Card/LanguageCard'
import IntroductionCard from '../../Card/IntroductionCard'
class CV extends Component {
    constructor(props) {
      super(props);
     
    }
    
    render() {
      
  
      return (
        <div className="CV">
                <IntroductionCard/>
                <ExperienceCard/>
                <ProjectCard/>
                <SkillCard/>
                <LanguageCard/>
                <EducationCard/>
                <DegreeCard/>
                <AwardCard/>
                <ActivityCard/>
                <PortfolioCard/>
                <PublicationCard/>
                <RecommendatorCard/>
        </div>
      );
    }
  }
  
  export default CV;