


import React from 'react';
import $ from 'jquery';
import { Link } from 'react-router';

export default class ListofItem extends React.Component {

      render() {
        const {create} = this.props;
          return (

            <div className="edu-history">
                <i className={this.props.icon}></i>
                  <div className="edu-hisinfo">
                    <div className="row">
                      <div className="col-md-11 col-sm-11 col-xs-10">
                        <span style={{color:'#8b91dd'}}>{this.props.name} - </span>
                          <span><i>{this.props.year}</i></span>
                          <span><i>{this.props.position}</i></span>
                      </div>
                        <div className="col-md-1 col-sm-1 col-xs-2">
                          <Link onClick={(e) => create(true)}><i className="fa fa-pencil pull-right"></i></Link>
                        </div>
                      </div>

                        <i> <span>{this.props.place}</span></i>
                        <i> <span>{this.props.level}</span></i>
                        <i> <span>{this.props.description}</span></i> 
                        <i> <span>{this.props.email}</span></i>
                        <i> <span>{this.props.relation}</span></i> 
                  </div>
            </div>
          );
      }
    }




export default class FollowItem extends React.Component {

      constructor(props){
        super(props);
        this.state = {
          icondown: true,
    
         
        }
      }
    
    
      toggle_widget() {
        $(document.getElementById(this.props.groupName)).next().slideToggle();
        document.getElementById(this.props.groupName).classList.toggle('active');
        document.getElementById(this.props.groupName).classList.toggle('closed');
        this.setState({icondown:!this.state.icondown});
      }
    
      toggleIcon=()=>{
        this.setState({icondown:!this.state.icondown});
      }
    
      renderList(valueArr){
            return valueArr.map(elem=>
                <ListofItem year={elem.year}
                            name={elem.name}
                            level={elem.level}
                            place={elem.place}
                            icon={elem.icon}
                            position={elem.position}
                            email={elem.email}
                            relation={elem.relation}
                            description={elem.description}
                            create={this.props}
                            
                />
          );
      }
    
      render(){
          const func = this.renderList(this.props.valueArr);
          const { create} = this.props;
        return (
    
         
            <div className="edu-history-sec" id={this.props.id}>
            <div className="row">
              <div className="col-md-11 col-sm-11 col-xs-10">
              <h2 className="sb-title open"
                id={this.props.groupName}
                onClick={() => this.toggle_widget()}
                >{this.props.name}
                <b onClick  = {()=>{this.toggleIcon()}}className={this.state.icondown?'fa fa-sort-up':'fa fa-sort-down'}>
                </b>
      
                
              </h2>
      
              <div className="specialism_widget">
              {func}
                </div>
                </div>
                <div className="col-md-1 col-sm-1 col-xs-2">
                <Link onClick={(e) => create(true)}><i className="fa fa-plus"></i></Link>
                </div>
              </div>
            </div>

        );
        ;
      }
    }