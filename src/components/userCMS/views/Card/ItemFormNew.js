import React, { Component } from 'react';
import request from 'superagent';
import { monthOptions, yearOptions , schoolOptions, majorOptions} from '../../../../commons/constants';

// import Select, { Creatable } from 'react-select';
// import 'react-select/dist/react-select.css';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

import TagsInput from 'react-tagsinput';
import 'react-tagsinput/react-tagsinput.css';

import Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';
import moment from 'moment';

import Toggle from 'react-toggle'
import "react-toggle/style.css"

import { languageArr } from '../../../../commons/constants';
import swal from 'sweetalert2';

const $ = window.jQuery;

const MIN_YEAR = 0;
const MAX_YEAR = 15;


export class IntroductionForm extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Introduction</label>
                    <div className="col-sm-10">
                        <textarea rows="5"
                            name="description"
                            value={item.description}
                            className="form-control"
                            style={{marginTop:10}}
                            onChange={(e) => handleChanged(e, "description")}>
                        </textarea>
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}

export class ExperienceForm extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>
               
               


                <div className="form-group">
                    <label className="col-sm-2 control-label">Position</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.position}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "position")}
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Company</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.company_name}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "company")} />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Location</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="location"
                            value={item.location}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "location")} />
                    </div>
                </div>

                <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label" style={{paddingLeft:0}}>Begin</label>
                    <div className="col-sm-5"  style={{paddingLeft:5}}>
                        <Select
                            className="profile-select"
                           
                            value={start.month}
                            options={monthOptions}
                            onChange={(e) => handleChanged(e, "school")}
                            placeholder="Choose month"
                        />
                    </div>
                    <div className="col-sm-5"  style={{ paddingRight:0}}>
                        <Select
                            className="profile-select"
                            value={start.year}
                            options={yearOptions()}
                            onChange={(e) => handleChanged(e, "startYear")}
                            placeholder="Choose year"
                        />
                    </div>
                </div>

                <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label" style={{paddingLeft:0}} htmlFor="end">End</label>
                    <div className="col-sm-5"  style={{paddingLeft:5}}>
                        <Select
                            className="profile-select"
                            value={end.month}
                            options={monthOptions}
                            onChange={(e) => handleChanged(e, "endMonth")}
                            placeholder="Choose month"
                        />
                    </div>
                    <div className="col-sm-5"  style={{paddingRight:0}}>
                        <Select
                            className="profile-select"
                            value={end.year}
                            options={yearOptions()}
                            onChange={(e) => handleChanged(e, "endYear")}
                            placeholder="Choose year"
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Description</label>
                    <div className="col-sm-10">
                        <textarea rows="5"
                            name="description"
                            value={item.description}
                            className="form-control"
                            style={{marginTop:10}}
                            onChange={(e) => handleChanged(e, "description")}>
                        </textarea>
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}


export class EducationForm extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>
               
               <div className="form-group col-lg-12" style={{paddingLeft:0,paddingRight:0}}>
                    <label className="control-label col-lg-2" >School</label>
                    <div>
                    <Select
                            className="profile-select col-lg-10"
                           
                            value={start.month}
                            options={schoolOptions}
                            onChange={(e) => handleChanged(e, "startMonth")}
                            placeholder="Choose school"
                        />
                    </div>
                    
                </div>


                 <div className="form-group col-lg-12" style={{paddingLeft:0,paddingRight:0}}>
                    <label className="control-label col-lg-2" >Major</label>
                    <div>
                    <Select
                            className="profile-select col-lg-10"
                           
                            value={start.month}
                            options={majorOptions}
                            onChange={(e) => handleChanged(e, "startMonth")}
                            placeholder="Choose major"
                        />
                    </div>
                    
                </div>



                <div className="form-group">
                    <label className="col-sm-2 control-label">GPA</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.GPA}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "position")}
                        />
                    </div>
                </div>

                <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label" style={{paddingLeft:0}}>Start year</label>
                    <div className="col-sm-4"  style={{paddingLeft:5}}>
                        <Select
                            className="profile-select"
                           
                            value={start.year}
                            options={yearOptions()}
                            onChange={(e) => handleChanged(e, "startYear")}
                            placeholder="Choose year"
                        />
                    </div>
                    <label className="col-sm-2 control-label" style={{paddingLeft:0}}>Start year</label>
                    <div className="col-sm-4"  style={{ paddingRight:0}}>
                        <Select
                            className="profile-select"
                            value={end.year}
                            options={yearOptions()}
                            onChange={(e) => handleChanged(e, "endYear")}
                            placeholder="Choose year"
                        />
                    </div>
                </div>

                <div className="form-group col-lg-12" style={{paddingLeft:0}}>
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I am studying here</label>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Description</label>
                    <div className="col-sm-10">
                        <textarea rows="5"
                            name="description"
                            value={item.description}
                            className="form-control"
                            style={{marginTop:10}}
                            onChange={(e) => handleChanged(e, "description")}>
                        </textarea>
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}



export class ProjectForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>
               
               <div className="form-group col-lg-12" style={{paddingLeft:0, paddingRight:0}}>
                    <label className="control-label col-lg-2" >Project</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.project}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "project")}
                        />
                    </div>
                    
                </div>



                <div className="form-group">
                    <label className="col-sm-2 control-label">Position</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.position}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "position")}
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Company</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.company_name}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "company")} />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Location</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="location"
                            value={item.location}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "location")} />
                    </div>
                </div>

                <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label" style={{paddingLeft:0}}>Begin</label>
                    <div className="col-sm-5"  style={{paddingLeft:5}}>
                        <Select
                            className="profile-select"
                           
                            value={start.month}
                            options={monthOptions}
                            onChange={(e) => handleChanged(e, "school")}
                            placeholder="Choose month"
                        />
                    </div>
                    <div className="col-sm-5"  style={{ paddingRight:0}}>
                        <Select
                            className="profile-select"
                            value={start.year}
                            options={yearOptions()}
                            onChange={(e) => handleChanged(e, "startYear")}
                            placeholder="Choose year"
                        />
                    </div>
                </div>

                <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label" style={{paddingLeft:0}} htmlFor="end">End</label>
                    <div className="col-sm-5"  style={{paddingLeft:5}}>
                        <Select
                            className="profile-select"
                            value={end.month}
                            options={monthOptions}
                            onChange={(e) => handleChanged(e, "endMonth")}
                            placeholder="Choose month"
                        />
                    </div>
                    <div className="col-sm-5"  style={{paddingRight:0}}>
                        <Select
                            className="profile-select"
                            value={end.year}
                            options={yearOptions()}
                            onChange={(e) => handleChanged(e, "endYear")}
                            placeholder="Choose year"
                        />
                    </div>
                </div>


                <div className="form-group col-lg-12" style={{paddingLeft:0}}>
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to join this</label>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Description</label>
                    <div className="col-sm-10">
                        <textarea rows="5"
                            name="description"
                            value={item.description}
                            className="form-control"
                            style={{marginTop:10}}
                            onChange={(e) => handleChanged(e, "description")}>
                        </textarea>
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}



export class SkillForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>
               
               <div className="form-group col-lg-12" style={{paddingLeft:0, paddingRight:0}}>
                    <label className="control-label col-lg-2" >Skill</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.skill}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "skill")}
                        />
                    </div>
                    
                </div>



                <div className="form-group">
                    <label className="col-sm-2 control-label">Level</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.level}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "level")}
                        />
                    </div>
                </div>

                <div className="form-group col-lg-12" style={{paddingLeft:0}}>
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to join this</label>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Description</label>
                    <div className="col-sm-10">
                        <textarea rows="5"
                            name="description"
                            value={item.description}
                            className="form-control"
                            style={{marginTop:10}}
                            onChange={(e) => handleChanged(e, "description")}>
                        </textarea>
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}


export class LanguageForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>
               
               <div className="form-group col-lg-12" style={{paddingLeft:0, paddingRight:0}}>
                    <label className="control-label col-lg-2" >Language</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.language}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "language")}
                        />
                    </div>
                    
                </div>



                <div className="form-group">
                    <label className="col-sm-2 control-label">Level</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.level}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "level")}
                        />
                    </div>
                </div>

                <div className="form-group col-lg-12" style={{paddingLeft:0}}>
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to join this</label>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Description</label>
                    <div className="col-sm-10">
                        <textarea rows="5"
                            name="description"
                            value={item.description}
                            className="form-control"
                            style={{marginTop:10}}
                            onChange={(e) => handleChanged(e, "description")}>
                        </textarea>
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}



export class DegreeForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>
               
               <div className="form-group col-lg-12" style={{paddingLeft:0, paddingRight:0}}>
                    <label className="control-label col-lg-2" >Organization</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.organization}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "organization")}
                        />
                    </div>
                    
                </div>



                <div className="form-group">
                    <label className="col-sm-2 control-label">Name</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.name}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "name")}
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Type</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.type}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "type")}
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Graduation date</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.date}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "date")}
                        />
                    </div>
                </div>

                 <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label" style={{paddingLeft:0}}>Score</label>
                    <div className="col-sm-10" style={{paddingLeft:4,paddingRight:0}}>
                        <input type="text"
                            name="title"
                            value={item.score}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "score")}
                        />
                    </div>
                </div>

                 <div className="form-group">
                    <label className="col-sm-2 control-label">Classification</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.classification}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "classification")}
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}


export class AwardForm extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>
               
               


                <div className="form-group">
                    <label className="col-sm-2 control-label">Name</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.name}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "name")}
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Organization</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.company_name}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "company")} />
                    </div>
                </div>

                 <div className="form-group">
                    <label className="col-sm-2 control-label">Organization Place</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.place}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "place")} />
                    </div>
                </div>

                <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label" style={{paddingLeft:0}}>Time</label>
                    <div className="col-sm-5"  style={{paddingLeft:5}}>
                        <Select
                            className="profile-select"
                           
                            value={start.month}
                            options={monthOptions}
                            onChange={(e) => handleChanged(e, "school")}
                            placeholder="Choose month"
                        />
                    </div>
                    <div className="col-sm-5"  style={{ paddingRight:0}}>
                        <Select
                            className="profile-select"
                            value={start.year}
                            options={yearOptions()}
                            onChange={(e) => handleChanged(e, "startYear")}
                            placeholder="Choose year"
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Description</label>
                    <div className="col-sm-10">
                        <textarea rows="5"
                            name="description"
                            value={item.description}
                            className="form-control"
                            style={{marginTop:10}}
                            onChange={(e) => handleChanged(e, "description")}>
                        </textarea>
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}


export class ActivitiesForm extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>
               
               


                <div className="form-group">
                    <label className="col-sm-2 control-label">Position</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.position}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "position")}
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Name</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.company_name}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "company")} />
                    </div>
                </div>


                <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label" style={{paddingLeft:0}}>Begin</label>
                    <div className="col-sm-5"  style={{paddingLeft:5}}>
                        <Select
                            className="profile-select"
                           
                            value={start.month}
                            options={monthOptions}
                            onChange={(e) => handleChanged(e, "school")}
                            placeholder="Choose month"
                        />
                    </div>
                    <div className="col-sm-5"  style={{ paddingRight:0}}>
                        <Select
                            className="profile-select"
                            value={start.year}
                            options={yearOptions()}
                            onChange={(e) => handleChanged(e, "startYear")}
                            placeholder="Choose year"
                        />
                    </div>
                </div>

                <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label" style={{paddingLeft:0}} htmlFor="end">End</label>
                    <div className="col-sm-5"  style={{paddingLeft:5}}>
                        <Select
                            className="profile-select"
                            value={end.month}
                            options={monthOptions}
                            onChange={(e) => handleChanged(e, "endMonth")}
                            placeholder="Choose month"
                        />
                    </div>
                    <div className="col-sm-5"  style={{paddingRight:0}}>
                        <Select
                            className="profile-select"
                            value={end.year}
                            options={yearOptions()}
                            onChange={(e) => handleChanged(e, "endYear")}
                            placeholder="Choose year"
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Description</label>
                    <div className="col-sm-10">
                        <textarea rows="5"
                            name="description"
                            value={item.description}
                            className="form-control"
                            style={{marginTop:10}}
                            onChange={(e) => handleChanged(e, "description")}>
                        </textarea>
                    </div>
                </div>

                <div className="form-group col-lg-12" style={{paddingLeft:0}}>
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I am doing this</label>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}



export class PortfolioForm extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>
               
               


                <div className="form-group">
                    <label className="col-sm-2 control-label">Name</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.name}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "name")}
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Image's link</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.company_name}
                            className=""
                            onChange={(e) => handleChanged(e, "company")} />
                    </div>
                </div>


                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}



export class RecommendatorForm extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>
               
               


                <div className="form-group">
                    <label className="col-sm-2 control-label">Name</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.position}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "position")}
                        />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Position</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.link}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "link")} />
                    </div>
                </div>

                 <div className="form-group">
                    <label className="col-sm-2 control-label">Company</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.company_name}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "company")} />
                    </div>
                </div>

                 <div className="form-group">
                    <label className="col-sm-2 control-label">Email</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.email}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "email")} />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Relation</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.relation}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "relation")} />
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}



export class PublicationForm extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            jobFieldOptions: [],
            school_name: [],
            isLoadingField3: true,
            isLoadingSchool: true,
        }
    }

    render() {
        const { item, handleChanged } = this.props;
        var start = (item.start) ? item.start : { month: "", year: "" };
        var end = (item.end) ? item.end : { month: "", year: "" };
        var school = (item.school) ? item.school : { school: ""};
        const { jobFieldOptions, school_name, isLoadingField3, isLoadingSchool } = this.state;
        return (
            <div>
               
               


                <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label">Publishing house</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="title"
                            value={item.name}
                            className="form-control"
                            placeholder=""
                            onChange={(e) => handleChanged(e, "name")}
                        />
                    </div>
                </div>

                <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label">Publish Date</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.date}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "date")} />
                    </div>
                </div>

                 <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label">Title</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.title}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "title")} />
                    </div>
                </div>

                 <div className="form-group col-lg-12">
                    <label className="col-sm-2 control-label">Co-publishing house</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="company"
                            value={item.company_name}
                            className="form-control"
                            onChange={(e) => handleChanged(e, "company")} />
                    </div>
                </div>


                <div className="form-group">
                    <label className="col-sm-2 control-label">
                        <Toggle
                            id="isPublic"
                            defaultChecked={item.isPublic}
                            onChange={(e) => handleChanged(e, "isPublic")} />
                    </label>
                    <label className="control-label col-sm-10 text-left" htmlFor="isPublic">I want to public this information</label>
                </div>

            </div>
        )
    }
}
















