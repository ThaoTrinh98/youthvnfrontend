import React from 'react';
import {Modal, Button} from 'react-bootstrap';
import {ExperienceForm,EducationForm, ProjectForm, PortfolioForm,PublicationForm,SkillForm,DegreeForm,ActivitiesForm,AwardForm,LanguageForm,RecommendatorForm,IntroductionForm} from './ItemFormNew';
//import {EventOrgForm} from './ItemFormNew';

export class ItemModal extends React.Component{

    render(){

        return (
            <div className="modal-container text-left usercms-modal" style={{zIndex: 100}}>
                <Modal show={this.props.showModal}
                        onHide={this.props.close}
                        container={this}
                >
                <Modal.Header  >
                <Modal.Title id="contained-modal-title" className="col-lg-10"><h1>{this.props.title}</h1></Modal.Title>
                </Modal.Header>
                {/*<form className="form-horizontal usercms-detail">*/}
                    <Modal.Body>
                    {(this.props.title === "Experience")? <ExperienceForm handleChanged={this.props.handleChanged} item={this.props.item} />
                    :(this.props.title === "Education")? <EducationForm handleChanged={this.props.handleChanged} item={this.props.item} />
                    :(this.props.title === "Project")? <ProjectForm handleChanged={this.props.handleChanged} item={this.props.item} />
                    :(this.props.title === "Skill")? <SkillForm handleChanged={this.props.handleChanged} item={this.props.item} />
                    :(this.props.title === "Activities")? <ActivitiesForm handleChanged={this.props.handleChanged} item={this.props.item} />
                    :(this.props.title === "Publication")? <PublicationForm handleChanged={this.props.handleChanged} item={this.props.item} />
                    :(this.props.title === "Degree")? <DegreeForm handleChanged={this.props.handleChanged} item={this.props.item} />
                    :(this.props.title === "Portfolio")? <PortfolioForm handleChanged={this.props.handleChanged} item={this.props.item} />
                    :(this.props.title === "Award")? <AwardForm handleChanged={this.props.handleChanged} item={this.props.item} />
                    :(this.props.title === "Language")? <LanguageForm handleChanged={this.props.handleChanged} item={this.props.item} />
                    :(this.props.title === "Recommendator")? <RecommendatorForm handleChanged={this.props.handleChanged} item={this.props.item} />
                    :<IntroductionForm handleChanged={this.props.handleChanged} item={this.props.item} />}
                        
                    </Modal.Body>
                    <Modal.Footer>

                        <Button href="/user/account/cv" type="button" className="btn btn-default btn-lg pull-right" style={{marginRight:13}}>Save</Button>

                        <Button href="/user/account/cv" type="button" className="btn btn-default btn-lg pull-right" style={{marginRight:13}}>Delete</Button>

                        <Button href="/user/account/cv" type="button" className="btn btn-default btn-lg pull-right" style={{marginRight:13}}>Return</Button>
                        
                    </Modal.Footer>
               {/* </form>*/}
                </Modal>
            </div>
        )

    }
}