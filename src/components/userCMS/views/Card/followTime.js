
import React from 'react';
import $ from 'jquery';
import { Link } from 'react-router';
import {ElementofTime} from './RenderItem'


export default class FollowTime extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      icondown: true,

     
    }
  }


  toggle_widget() {
    $(document.getElementById(this.props.groupName)).next().slideToggle();
    document.getElementById(this.props.groupName).classList.toggle('active');
    document.getElementById(this.props.groupName).classList.toggle('closed');
    this.setState({icondown:!this.state.icondown});
  }

  toggleIcon=()=>{
    this.setState({icondown:!this.state.icondown});
  }

  

  render(){
      const valueArr= this.props.valueArr;
      const { create} = this.props;
    return (

     
      <div className="edu-history-sec" id={this.props.id}>
        <div className="row">
          <div className="col-md-11 col-sm-11 col-xs-10">
            <h2 className="sb-title open"
              id={this.props.groupName}
              onClick={() => this.toggle_widget()}
              >{this.props.name}
              <b onClick  = {()=>{this.toggleIcon()}}className={this.state.icondown?'fa fa-sort-up':'fa fa-sort-down'}>
              </b>
            </h2>
            <div className="specialism_widget">
              {valueArr.map(elem=>
            <ElementofTime year={elem.year}
                        name={elem.name}
                        description={elem.description}
                        place={elem.place}
                        create={this.props.create}
              />)}
                </div>
                </div>
                <div className="col-md-1 col-sm-1 col-xs-2">
                <Link onClick={(e) => create(true)}><i className="fa fa-plus" style={{marginTop:"70%"}}></i></Link>
                </div>
              </div>
            </div>
      
       
    );
    ;
  }
}